this.showLoadFile();

var loki = require("lokijs");
var db = new loki("loki.json");
var fileExists = require('file-exists');
var read = require('read-file-utf8');
var data = read('./loki.json');
db.loadJSON(data);

/**
 * Verifica se o banco de dados exiiste
 */
fileExists('./loki.json').then(exists => {
    if (exists == false) {
        console.log("========fileExists()========");
        console.log("Criando banco de dados...");
        console.log("========fileExists()========");
        db.addCollection('clientes');
        db.addCollection('produtos');
        db.addCollection('vendas');
        db.addCollection('notas');
        db.addCollection('empresas');
        db.addCollection('cidadesestados');
        db.addCollection('estados');
        db.addCollection('ncms');
        db.addCollection('cnaes');
        db.addCollection('cfops');
        db.addCollection('tabelacests');
        db.save();
        console.log("========fileExists()========");
        console.log("Banco de dados criado com sucesso...");
        console.log("========fileExists()========");
    } else {
        console.log("========fileExists()========");
        console.log("O banco de dados já está criado e rodando...");
        console.log("========fileExists()========");
    }

    if (!db.getCollection('clientes')) {
        var clientes = db.addCollection('clientes');
        db.save();
        alert('Coleção de clientes criada com sucesso...');
    }

    if (!db.getCollection('produtos')) {
        var produtos = db.addCollection('produtos');
        db.save();
        alert('Coleção de produtos criada com sucesso...');
    }

    if (!db.getCollection('vendas')) {
        var vendas = db.addCollection('vendas');
        db.save();
        alert('Coleção de vendas criada com sucesso...');
    }

    if (!db.getCollection('notas')) {
        var notas = db.addCollection('notas');
        db.save();
        alert('Coleção de notas criada com sucesso...');
    }
    if (!db.getCollection('empresas')) {
        var empresas = db.addCollection('empresas');
        db.save();
        alert('Coleção de empresas criada com sucesso...');
    }
    if (!db.getCollection('cidadesestados')) {
        var cidadesestados = db.addCollection('cidadesestados');
        db.save();
        alert('Coleção de cidades e estados criada com sucesso...');
    }
    if (!db.getCollection('estados')) {
        var estados = db.addCollection('estados');
        db.save();
        alert('Coleção de estados criada com sucesso...');
    }
    if (!db.getCollection('ncms')) {
        var ncms = db.addCollection('ncms');
        db.save();
        alert('Coleção de ncms criada com sucesso...');
    }
    if (!db.getCollection('cnaes')) {
        var cnaes = db.addCollection('cnaes');
        db.save();
        alert('Coleção de cnaes criada com sucesso...');
    }
    if (!db.getCollection('cfops')) {
        var cfops = db.addCollection('cfops');
        db.save();
        alert('Coleção de cfops criada com sucesso...');
    }
    if (!db.getCollection('tabelacests')) {
        var tabelacests = db.addCollection('tabelacests');
        db.save();
        alert('Coleção de tabelacests criada com sucesso...');
    }

});

function showLoadFile() {
    console.log("========showLoadFile()========");
    console.log("main carregado com sucesso...");
    console.log("========showLoadFile()========");
};