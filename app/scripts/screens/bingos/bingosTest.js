window.Vue = require('vue');
const HTML5ToPDF = require("html5-to-pdf")
const path = require("path")

const run = async () => {
    const html5ToPDF = new HTML5ToPDF({
        inputPath: path.join(__dirname, "bingosGenerated.html"),
        outputPath: path.join(__dirname, "tmp", "output.pdf"),
        templatePath: path.join(__dirname),
        include: [
            // path.join(__dirname, "templates", "photon.css"),
            // path.join(__dirname, "bootstrap4/css", "bootstrap.min.css"),
        ],
    })

    await html5ToPDF.start()
    await html5ToPDF.build()
    await html5ToPDF.close()
    console.log("DONE")
    alert('Arquivo gerado com sucesso...');
    // process.exit(0)
}

//   run();


new Vue({
    el: 'html',
    data: {
        html: '',
        dateB: '',
        hour: '',
        value: '',
        bootstrap: false,
        debug: false,
        qtdBingos: 1,
        cartelas: []
    },

    ready: function () {
        this.bootstrap = false;
    },
    methods: {
        makeDebug: function () {
            if (this.debug == false) {
                this.debug = true;
            } else {
                this.debug = false;
            }
        },
        cleanBingos: function () {
            this.html = '';
            this.cartelas = []
            var fs = require('fs');
            let pathTo = path.join(__dirname)
            fs.writeFile(pathTo + '/bingosGenerated.html', '', function (err) {
                if (err) {
                    alert('write pdf file error', err);
                }
            })

            fs.writeFile(pathTo + '/tmp/output.pdf', '', function (err) {
                if (err) {
                    alert('write pdf file error', err);
                }
            })
            alert('Tudo limpo...');
        },
        pdfWindow: function () {
            const { BrowserWindow } = require('electron').remote
            const PDFWindow = require('electron-pdf-window')
            const win = new BrowserWindow({ width: 1400, height: 800 })
            PDFWindow.addSupport(win)
            // win.loadURL('http://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf')
            let pathTo = path.join(__dirname, "tmp")
            console.log(pathTo + '/output.pdf')
            win.loadURL(pathTo + '/output.pdf')
        },
        printView: function () {
            const { BrowserWindow } = require('electron').remote
            var fs = require('fs');
            const win = new BrowserWindow({ width: 1024, height: 800 })
            let pathTo = path.join(__dirname)
            console.log(pathTo)
            win.loadURL(pathTo + '/bingosGenerated.html')

            win.webContents.on("did-finish-load", function () {
                win.webContents.print({}, function (err, data) {
                    if (err) throw error;
                });
            });

            // win.webContents.on("did-finish-load", function () {
            //     // Use default printing options
            //     win.webContents.printToPDF({

            //     }, function (error, data) {
            //         if (error) throw error;
            //         let pathWrite = path.join(__dirname)
            //         fs.writeFile(pathWrite + '/tmp/output.pdf', data, function (err) {
            //             if (err)
            //                 alert('write pdf file error', err);
            //         })
            //     })
            // });
        },
        viewNormal: function () {
            const { BrowserWindow } = require('electron').remote
            var fs = require('fs');
            const win = new BrowserWindow({ width: 1024, height: 800 })
            let pathTo = path.join(__dirname)
            console.log(pathTo)
            win.loadURL(pathTo + '/bingosGenerated.html')
        },
        generateBingos: function () {
            // if (this.bootstrap == false) {
            //     this.bootstrap = true;
            // } else {
            //     this.bootstrap = false;
            // }

            for (var qtd = 1; qtd <= this.qtdBingos; qtd++) {

                /**
                 * Bloco de definição da linha 1
                 */
                let linha1_coluna1 = Math.floor(Math.random() * 74 + 1);
                let linha1_coluna2 = Math.floor(Math.random() * 74 + 1);
                let linha1_coluna3 = Math.floor(Math.random() * 74 + 1);
                let linha1_coluna4 = Math.floor(Math.random() * 74 + 1);
                let linha1_coluna5 = Math.floor(Math.random() * 74 + 1);

                /**
                 * Bloco de definição da linha 2
                 */
                let linha2_coluna1 = Math.floor(Math.random() * 74 + 1);
                let linha2_coluna2 = Math.floor(Math.random() * 74 + 1);
                let linha2_coluna3 = Math.floor(Math.random() * 74 + 1);
                let linha2_coluna4 = Math.floor(Math.random() * 74 + 1);
                let linha2_coluna5 = Math.floor(Math.random() * 74 + 1);

                /**
                 * Bloco de definição da linha 3
                 */
                let linha3_coluna1 = Math.floor(Math.random() * 74 + 1);
                let linha3_coluna2 = Math.floor(Math.random() * 74 + 1);
                let linha3_coluna3 = Math.floor(Math.random() * 74 + 1);
                let linha3_coluna4 = Math.floor(Math.random() * 74 + 1);
                let linha3_coluna5 = Math.floor(Math.random() * 74 + 1);

                /**
                 * Bloco de definição da linha 4
                 */
                let linha4_coluna1 = Math.floor(Math.random() * 74 + 1);
                let linha4_coluna2 = Math.floor(Math.random() * 74 + 1);
                let linha4_coluna3 = Math.floor(Math.random() * 74 + 1);
                let linha4_coluna4 = Math.floor(Math.random() * 74 + 1);
                let linha4_coluna5 = Math.floor(Math.random() * 74 + 1);

                /**
                 * Bloco de definição da linha 5
                 */
                let linha5_coluna1 = Math.floor(Math.random() * 74 + 1);
                let linha5_coluna2 = Math.floor(Math.random() * 74 + 1);
                let linha5_coluna3 = Math.floor(Math.random() * 74 + 1);
                let linha5_coluna4 = Math.floor(Math.random() * 74 + 1);
                let linha5_coluna5 = Math.floor(Math.random() * 74 + 1);

                /**
                 * Bloco da linha 1
                 */
                if (
                    linha1_coluna1 == linha1_coluna2
                    || linha1_coluna1 == linha1_coluna3
                    || linha1_coluna1 == linha1_coluna4
                    || linha1_coluna1 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha1_coluna1 == linha2_coluna2
                    || linha1_coluna1 == linha2_coluna3
                    || linha1_coluna1 == linha2_coluna4
                    || linha1_coluna1 == linha2_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha1_coluna1 == linha3_coluna2
                    || linha1_coluna1 == linha3_coluna3
                    || linha1_coluna1 == linha3_coluna4
                    || linha1_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha1_coluna1 == linha4_coluna2
                    || linha1_coluna1 == linha4_coluna3
                    || linha1_coluna1 == linha4_coluna4
                    || linha1_coluna1 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha1_coluna1 == linha5_coluna2
                    || linha1_coluna1 == linha5_coluna3
                    || linha1_coluna1 == linha5_coluna4
                    || linha1_coluna1 == linha5_coluna5
                    || linha1_coluna1 == 0
                ) {
                    linha1_coluna1 += 1;
                }

                if (linha1_coluna2 == linha1_coluna1
                    || linha1_coluna2 == linha1_coluna3
                    || linha1_coluna2 == linha1_coluna4
                    || linha1_coluna2 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha1_coluna2 == linha2_coluna1
                    || linha1_coluna2 == linha2_coluna3
                    || linha1_coluna2 == linha2_coluna4
                    || linha1_coluna2 == linha2_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha1_coluna2 == linha3_coluna1
                    || linha1_coluna2 == linha3_coluna3
                    || linha1_coluna2 == linha3_coluna4
                    || linha1_coluna2 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha1_coluna2 == linha4_coluna1
                    || linha1_coluna2 == linha4_coluna3
                    || linha1_coluna2 == linha4_coluna4
                    || linha1_coluna2 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha1_coluna2 == linha5_coluna1
                    || linha1_coluna2 == linha5_coluna3
                    || linha1_coluna2 == linha5_coluna4
                    || linha1_coluna2 == linha5_coluna5
                    || linha1_coluna2 == 0) {
                    linha1_coluna2 += 1;
                }

                if (linha1_coluna3 == linha1_coluna1
                    || linha1_coluna3 == linha1_coluna2
                    || linha1_coluna3 == linha1_coluna4
                    || linha1_coluna3 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha1_coluna3 == linha2_coluna1
                    || linha1_coluna3 == linha2_coluna2
                    || linha1_coluna3 == linha2_coluna4
                    || linha1_coluna3 == linha2_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha1_coluna3 == linha3_coluna1
                    || linha1_coluna3 == linha3_coluna2
                    || linha1_coluna3 == linha3_coluna4
                    || linha1_coluna3 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha1_coluna3 == linha4_coluna1
                    || linha1_coluna3 == linha4_coluna2
                    || linha1_coluna3 == linha4_coluna4
                    || linha1_coluna3 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha1_coluna3 == linha5_coluna1
                    || linha1_coluna3 == linha5_coluna2
                    || linha1_coluna3 == linha5_coluna4
                    || linha1_coluna3 == linha5_coluna5
                    || linha1_coluna3 == 0) {
                    linha1_coluna3 += 1;
                }

                if (linha1_coluna4 == linha1_coluna1
                    || linha1_coluna4 == linha1_coluna2
                    || linha1_coluna4 == linha1_coluna3
                    || linha1_coluna4 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha1_coluna4 == linha2_coluna1
                    || linha1_coluna4 == linha2_coluna2
                    || linha1_coluna4 == linha2_coluna3
                    || linha1_coluna4 == linha2_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha1_coluna4 == linha3_coluna1
                    || linha1_coluna4 == linha3_coluna2
                    || linha1_coluna4 == linha3_coluna3
                    || linha1_coluna4 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha1_coluna4 == linha4_coluna1
                    || linha1_coluna4 == linha4_coluna2
                    || linha1_coluna4 == linha4_coluna3
                    || linha1_coluna4 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha1_coluna4 == linha5_coluna1
                    || linha1_coluna4 == linha5_coluna2
                    || linha1_coluna4 == linha5_coluna3
                    || linha1_coluna4 == linha5_coluna5
                    || linha1_coluna4 == 0) {
                    linha1_coluna4 += 1;
                }

                if (linha1_coluna5 == linha1_coluna1
                    || linha1_coluna5 == linha1_coluna2
                    || linha1_coluna5 == linha1_coluna3
                    || linha1_coluna5 == linha1_coluna4
                    //Evitar chances de aparecer na linha 2
                    || linha1_coluna5 == linha2_coluna1
                    || linha1_coluna5 == linha2_coluna2
                    || linha1_coluna5 == linha2_coluna3
                    || linha1_coluna5 == linha2_coluna4
                    //Evitar chances de aparecer na linha 3
                    || linha1_coluna5 == linha3_coluna1
                    || linha1_coluna5 == linha3_coluna2
                    || linha1_coluna5 == linha3_coluna3
                    || linha1_coluna5 == linha3_coluna4
                    //Evitar chances de aparecer na linha 4
                    || linha1_coluna5 == linha4_coluna1
                    || linha1_coluna5 == linha4_coluna2
                    || linha1_coluna5 == linha4_coluna3
                    || linha1_coluna5 == linha4_coluna4
                    //Evitar chances de aparecer na linha 5
                    || linha1_coluna5 == linha5_coluna1
                    || linha1_coluna5 == linha5_coluna2
                    || linha1_coluna5 == linha5_coluna3
                    || linha1_coluna5 == linha5_coluna4
                    || linha1_coluna5 == 0) {
                    linha1_coluna5 += 1;
                }

                /**
                 * Bloco da coluna 1
                 */
                if (
                    linha1_coluna1 == linha2_coluna1
                    || linha1_coluna1 == linha3_coluna1
                    || linha1_coluna1 == linha4_coluna1
                    || linha1_coluna1 == linha5_coluna1
                    || linha1_coluna1 == 0
                ) {
                    linha1_coluna1 += 1;
                }

                if (
                    linha2_coluna1 == linha1_coluna1
                    || linha2_coluna1 == linha3_coluna1
                    || linha2_coluna1 == linha4_coluna1
                    || linha2_coluna1 == linha5_coluna1
                    || linha2_coluna1 == 0
                ) {
                    linha2_coluna1 += 1;
                }

                if (
                    linha3_coluna1 == linha1_coluna1
                    || linha3_coluna1 == linha2_coluna1
                    || linha3_coluna1 == linha4_coluna1
                    || linha3_coluna1 == linha5_coluna1
                    || linha3_coluna1 == 0
                ) {
                    linha3_coluna1 += 1;
                }

                if (
                    linha4_coluna1 == linha1_coluna1
                    || linha4_coluna1 == linha2_coluna1
                    || linha4_coluna1 == linha3_coluna1
                    || linha4_coluna1 == linha5_coluna1
                    || linha4_coluna1 == 0
                ) {
                    linha4_coluna1 += 1;
                }

                if (
                    linha5_coluna1 == linha1_coluna1
                    || linha5_coluna1 == linha2_coluna1
                    || linha5_coluna1 == linha3_coluna1
                    || linha5_coluna1 == linha4_coluna1
                    || linha5_coluna1 == 0
                ) {
                    linha5_coluna1 += 1;
                }

                /**
                * Bloco da linha 2
                */
                if (linha2_coluna1 == linha2_coluna2
                    || linha2_coluna1 == linha2_coluna3
                    || linha2_coluna1 == linha2_coluna4
                    || linha2_coluna1 == linha2_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha2_coluna1 == linha1_coluna2
                    || linha2_coluna1 == linha1_coluna3
                    || linha2_coluna1 == linha1_coluna4
                    || linha2_coluna1 == linha1_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha2_coluna1 == linha3_coluna2
                    || linha2_coluna1 == linha3_coluna3
                    || linha2_coluna1 == linha3_coluna4
                    || linha2_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha2_coluna1 == linha4_coluna2
                    || linha2_coluna1 == linha4_coluna3
                    || linha2_coluna1 == linha4_coluna4
                    || linha2_coluna1 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha2_coluna1 == linha5_coluna2
                    || linha2_coluna1 == linha5_coluna3
                    || linha2_coluna1 == linha5_coluna4
                    || linha2_coluna1 == linha5_coluna5
                    || linha2_coluna1 == 0) {
                    linha2_coluna1 += 1;
                }

                if (linha2_coluna2 == linha2_coluna1
                    || linha2_coluna2 == linha2_coluna3
                    || linha2_coluna2 == linha2_coluna4
                    || linha2_coluna2 == linha2_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha2_coluna2 == linha1_coluna1
                    || linha2_coluna2 == linha1_coluna3
                    || linha2_coluna2 == linha1_coluna4
                    || linha2_coluna2 == linha1_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha2_coluna2 == linha3_coluna1
                    || linha2_coluna2 == linha3_coluna3
                    || linha2_coluna2 == linha3_coluna4
                    || linha2_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha2_coluna2 == linha4_coluna1
                    || linha2_coluna2 == linha4_coluna3
                    || linha2_coluna2 == linha4_coluna4
                    || linha2_coluna2 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha2_coluna2 == linha5_coluna1
                    || linha2_coluna2 == linha5_coluna3
                    || linha2_coluna2 == linha5_coluna4
                    || linha2_coluna2 == linha5_coluna5
                    || linha2_coluna2 == 0) {
                    linha2_coluna2 += 1;
                }

                if (linha2_coluna3 == linha2_coluna1
                    || linha2_coluna3 == linha2_coluna2
                    || linha2_coluna3 == linha2_coluna4
                    || linha2_coluna3 == linha2_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha2_coluna3 == linha1_coluna1
                    || linha2_coluna3 == linha1_coluna2
                    || linha2_coluna3 == linha1_coluna4
                    || linha2_coluna3 == linha1_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha2_coluna3 == linha3_coluna1
                    || linha2_coluna3 == linha3_coluna2
                    || linha2_coluna3 == linha3_coluna4
                    || linha2_coluna3 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha2_coluna3 == linha4_coluna1
                    || linha2_coluna3 == linha4_coluna2
                    || linha2_coluna3 == linha4_coluna4
                    || linha2_coluna3 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha2_coluna3 == linha5_coluna1
                    || linha2_coluna3 == linha5_coluna2
                    || linha2_coluna3 == linha5_coluna4
                    || linha2_coluna3 == linha5_coluna5
                    || linha2_coluna3 == 0) {
                    linha2_coluna3 += 1;
                }

                if (linha2_coluna4 == linha2_coluna1
                    || linha2_coluna4 == linha2_coluna2
                    || linha2_coluna4 == linha2_coluna3
                    || linha2_coluna4 == linha2_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha2_coluna4 == linha1_coluna1
                    || linha2_coluna4 == linha1_coluna2
                    || linha2_coluna4 == linha1_coluna3
                    || linha2_coluna4 == linha1_coluna5
                    //Evitar chances de aparecer na linha 3
                    || linha2_coluna4 == linha3_coluna1
                    || linha2_coluna4 == linha3_coluna2
                    || linha2_coluna4 == linha3_coluna3
                    || linha2_coluna4 == linha3_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha2_coluna4 == linha4_coluna1
                    || linha2_coluna4 == linha4_coluna2
                    || linha2_coluna4 == linha4_coluna3
                    || linha2_coluna4 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha2_coluna4 == linha5_coluna1
                    || linha2_coluna4 == linha5_coluna2
                    || linha2_coluna4 == linha5_coluna3
                    || linha2_coluna4 == linha5_coluna5
                    || linha2_coluna4 == 0) {
                    linha2_coluna4 += 1;
                }

                if (linha2_coluna5 == linha2_coluna1
                    || linha2_coluna5 == linha2_coluna2
                    || linha2_coluna5 == linha2_coluna3
                    || linha2_coluna5 == linha2_coluna4
                    //Evitar chances de aparecer na linha 1
                    || linha2_coluna5 == linha1_coluna1
                    || linha2_coluna5 == linha1_coluna2
                    || linha2_coluna5 == linha1_coluna3
                    || linha2_coluna5 == linha1_coluna4
                    //Evitar chances de aparecer na linha 3
                    || linha2_coluna5 == linha3_coluna1
                    || linha2_coluna5 == linha3_coluna2
                    || linha2_coluna5 == linha3_coluna3
                    || linha2_coluna5 == linha3_coluna4
                    //Evitar chances de aparecer na linha 4
                    || linha2_coluna5 == linha4_coluna1
                    || linha2_coluna5 == linha4_coluna2
                    || linha2_coluna5 == linha4_coluna3
                    || linha2_coluna5 == linha4_coluna4
                    //Evitar chances de aparecer na linha 5
                    || linha2_coluna5 == linha5_coluna1
                    || linha2_coluna5 == linha5_coluna2
                    || linha2_coluna5 == linha5_coluna3
                    || linha2_coluna5 == linha5_coluna4
                    || linha2_coluna5 == 0) {
                    linha2_coluna5 += 1;
                }

                /**
                * Bloco da coluna 2
                */
                if (
                    linha1_coluna2 == linha2_coluna2
                    || linha1_coluna2 == linha3_coluna2
                    || linha1_coluna2 == linha4_coluna2
                    || linha1_coluna2 == linha5_coluna2
                    || linha1_coluna2 == 0
                ) {
                    linha1_coluna2 += 1;
                }

                if (
                    linha2_coluna2 == linha1_coluna2
                    || linha2_coluna2 == linha3_coluna2
                    || linha2_coluna2 == linha4_coluna2
                    || linha2_coluna2 == linha5_coluna2
                    || linha2_coluna2 == 0
                ) {
                    linha2_coluna2 += 1;
                }

                if (
                    linha3_coluna2 == linha1_coluna2
                    || linha3_coluna2 == linha2_coluna2
                    || linha3_coluna2 == linha4_coluna2
                    || linha3_coluna2 == linha5_coluna2
                    || linha3_coluna2 == 0
                ) {
                    linha3_coluna2 += 1;
                }

                if (
                    linha4_coluna2 == linha1_coluna2
                    || linha4_coluna2 == linha2_coluna2
                    || linha4_coluna2 == linha3_coluna2
                    || linha4_coluna2 == linha5_coluna2
                    || linha4_coluna2 == 0
                ) {
                    linha4_coluna2 += 1;
                }

                if (
                    linha5_coluna2 == linha1_coluna2
                    || linha5_coluna2 == linha2_coluna2
                    || linha5_coluna2 == linha3_coluna2
                    || linha5_coluna2 == linha4_coluna2
                    || linha5_coluna2 == 0
                ) {
                    linha5_coluna2 += 1;
                }

                /**
                * Bloco da linha 3
                */
                if (linha3_coluna1 == linha3_coluna2
                    || linha3_coluna1 == linha3_coluna3
                    || linha3_coluna1 == linha3_coluna4
                    || linha3_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha3_coluna1 == linha1_coluna2
                    || linha3_coluna1 == linha1_coluna3
                    || linha3_coluna1 == linha1_coluna4
                    || linha3_coluna1 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha3_coluna1 == linha2_coluna2
                    || linha3_coluna1 == linha2_coluna3
                    || linha3_coluna1 == linha2_coluna4
                    || linha3_coluna1 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha3_coluna1 == linha4_coluna2
                    || linha3_coluna1 == linha4_coluna3
                    || linha3_coluna1 == linha4_coluna4
                    || linha3_coluna1 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha3_coluna1 == linha5_coluna2
                    || linha3_coluna1 == linha5_coluna3
                    || linha3_coluna1 == linha5_coluna4
                    || linha3_coluna1 == linha5_coluna5
                    || linha3_coluna1 == 0) {
                    linha3_coluna1 += 1;
                }

                if (linha3_coluna2 == linha3_coluna1
                    || linha3_coluna2 == linha3_coluna3
                    || linha3_coluna2 == linha3_coluna4
                    || linha3_coluna2 == linha3_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha3_coluna2 == linha1_coluna1
                    || linha3_coluna2 == linha1_coluna3
                    || linha3_coluna2 == linha1_coluna4
                    || linha3_coluna2 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha3_coluna2 == linha2_coluna1
                    || linha3_coluna2 == linha2_coluna3
                    || linha3_coluna2 == linha2_coluna4
                    || linha3_coluna2 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha3_coluna2 == linha4_coluna1
                    || linha3_coluna2 == linha4_coluna3
                    || linha3_coluna2 == linha4_coluna4
                    || linha3_coluna2 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha3_coluna2 == linha5_coluna1
                    || linha3_coluna2 == linha5_coluna3
                    || linha3_coluna2 == linha5_coluna4
                    || linha3_coluna2 == linha5_coluna5
                    || linha3_coluna2 == 0) {
                    linha3_coluna2 += 1;
                }

                if (linha3_coluna3 == linha3_coluna1
                    || linha3_coluna3 == linha3_coluna2
                    || linha3_coluna3 == linha3_coluna4
                    || linha3_coluna3 == linha3_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha3_coluna3 == linha1_coluna1
                    || linha3_coluna3 == linha1_coluna2
                    || linha3_coluna3 == linha1_coluna4
                    || linha3_coluna3 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha3_coluna3 == linha2_coluna1
                    || linha3_coluna3 == linha2_coluna2
                    || linha3_coluna3 == linha2_coluna4
                    || linha3_coluna3 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha3_coluna3 == linha4_coluna1
                    || linha3_coluna3 == linha4_coluna2
                    || linha3_coluna3 == linha4_coluna4
                    || linha3_coluna3 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha3_coluna3 == linha5_coluna1
                    || linha3_coluna3 == linha5_coluna2
                    || linha3_coluna3 == linha5_coluna4
                    || linha3_coluna3 == linha5_coluna5
                    || linha3_coluna3 == 0) {
                    linha3_coluna3 += 1;
                }

                if (linha3_coluna4 == linha3_coluna1
                    || linha3_coluna4 == linha3_coluna2
                    || linha3_coluna4 == linha3_coluna3
                    || linha3_coluna4 == linha3_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha3_coluna4 == linha1_coluna1
                    || linha3_coluna4 == linha1_coluna2
                    || linha3_coluna4 == linha1_coluna3
                    || linha3_coluna4 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha3_coluna4 == linha2_coluna1
                    || linha3_coluna4 == linha2_coluna2
                    || linha3_coluna4 == linha2_coluna3
                    || linha3_coluna4 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha3_coluna4 == linha4_coluna1
                    || linha3_coluna4 == linha4_coluna2
                    || linha3_coluna4 == linha4_coluna3
                    || linha3_coluna4 == linha4_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha3_coluna4 == linha5_coluna1
                    || linha3_coluna4 == linha5_coluna2
                    || linha3_coluna4 == linha5_coluna3
                    || linha3_coluna4 == linha5_coluna5
                    || linha3_coluna4 == 0) {
                    linha3_coluna4 += 1;
                }

                if (linha3_coluna5 == linha3_coluna1
                    || linha3_coluna5 == linha3_coluna2
                    || linha3_coluna5 == linha3_coluna3
                    || linha3_coluna5 == linha3_coluna4
                    //Evitar chances de aparecer na linha 1
                    || linha3_coluna5 == linha1_coluna1
                    || linha3_coluna5 == linha1_coluna2
                    || linha3_coluna5 == linha1_coluna3
                    || linha3_coluna5 == linha1_coluna4
                    //Evitar chances de aparecer na linha 2
                    || linha3_coluna5 == linha2_coluna1
                    || linha3_coluna5 == linha2_coluna2
                    || linha3_coluna5 == linha2_coluna3
                    || linha3_coluna5 == linha2_coluna4
                    //Evitar chances de aparecer na linha 4
                    || linha3_coluna5 == linha4_coluna1
                    || linha3_coluna5 == linha4_coluna2
                    || linha3_coluna5 == linha4_coluna3
                    || linha3_coluna5 == linha4_coluna4
                    //Evitar chances de aparecer na linha 5
                    || linha3_coluna5 == linha5_coluna1
                    || linha3_coluna5 == linha5_coluna2
                    || linha3_coluna5 == linha5_coluna3
                    || linha3_coluna5 == linha5_coluna4
                    || linha3_coluna5 == 0) {
                    linha3_coluna5 += 1;
                }

                /**
               * Bloco da coluna 3
               */
                if (
                    linha1_coluna3 == linha2_coluna3
                    || linha1_coluna3 == linha3_coluna3
                    || linha1_coluna3 == linha4_coluna3
                    || linha1_coluna3 == linha5_coluna3
                    || linha1_coluna3 == 0
                ) {
                    linha1_coluna3 += 1;
                }

                if (
                    linha2_coluna3 == linha1_coluna3
                    || linha2_coluna3 == linha3_coluna3
                    || linha2_coluna3 == linha4_coluna3
                    || linha2_coluna3 == linha5_coluna3
                    || linha2_coluna3 == 0
                ) {
                    linha2_coluna3 += 1;
                }

                if (
                    linha3_coluna3 == linha1_coluna3
                    || linha3_coluna3 == linha2_coluna3
                    || linha3_coluna3 == linha4_coluna3
                    || linha3_coluna3 == linha5_coluna3
                    || linha3_coluna3 == 0
                ) {
                    linha3_coluna3 += 1;
                }

                if (
                    linha4_coluna3 == linha1_coluna3
                    || linha4_coluna3 == linha2_coluna3
                    || linha4_coluna3 == linha3_coluna3
                    || linha4_coluna3 == linha5_coluna3
                    || linha4_coluna3 == 0
                ) {
                    linha4_coluna3 += 1;
                }

                if (
                    linha5_coluna3 == linha1_coluna3
                    || linha5_coluna3 == linha2_coluna3
                    || linha5_coluna3 == linha3_coluna3
                    || linha5_coluna3 == linha4_coluna3
                    || linha5_coluna3 == 0
                ) {
                    linha5_coluna3 += 1;
                }

                /**
                * Bloco da linha 4
                */
                if (linha4_coluna1 == linha4_coluna2
                    || linha4_coluna1 == linha4_coluna3
                    || linha4_coluna1 == linha4_coluna4
                    || linha4_coluna1 == linha4_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha4_coluna1 == linha1_coluna2
                    || linha4_coluna1 == linha1_coluna3
                    || linha4_coluna1 == linha1_coluna4
                    || linha4_coluna1 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha4_coluna1 == linha2_coluna2
                    || linha4_coluna1 == linha2_coluna3
                    || linha4_coluna1 == linha2_coluna4
                    || linha4_coluna1 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha4_coluna1 == linha3_coluna2
                    || linha4_coluna1 == linha3_coluna3
                    || linha4_coluna1 == linha3_coluna4
                    || linha4_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha4_coluna1 == linha5_coluna2
                    || linha4_coluna1 == linha5_coluna3
                    || linha4_coluna1 == linha5_coluna4
                    || linha4_coluna1 == linha5_coluna5
                    || linha4_coluna1 == 0) {
                    linha4_coluna1 += 1;
                }

                if (linha4_coluna2 == linha4_coluna1
                    || linha4_coluna2 == linha4_coluna3
                    || linha4_coluna2 == linha4_coluna4
                    || linha4_coluna2 == linha4_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha4_coluna2 == linha1_coluna1
                    || linha4_coluna2 == linha1_coluna3
                    || linha4_coluna2 == linha1_coluna4
                    || linha4_coluna2 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha4_coluna2 == linha2_coluna1
                    || linha4_coluna2 == linha2_coluna3
                    || linha4_coluna2 == linha2_coluna4
                    || linha4_coluna2 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha4_coluna2 == linha3_coluna1
                    || linha4_coluna2 == linha3_coluna3
                    || linha4_coluna2 == linha3_coluna4
                    || linha4_coluna2 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha4_coluna2 == linha5_coluna1
                    || linha4_coluna2 == linha5_coluna3
                    || linha4_coluna2 == linha5_coluna4
                    || linha4_coluna2 == linha5_coluna5
                    || linha4_coluna2 == 0) {
                    linha4_coluna2 += 1;
                }

                if (linha4_coluna3 == linha4_coluna1
                    || linha4_coluna3 == linha4_coluna2
                    || linha4_coluna3 == linha4_coluna4
                    || linha4_coluna3 == linha4_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha4_coluna3 == linha1_coluna1
                    || linha4_coluna3 == linha1_coluna2
                    || linha4_coluna3 == linha1_coluna4
                    || linha4_coluna3 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha4_coluna3 == linha2_coluna1
                    || linha4_coluna3 == linha2_coluna2
                    || linha4_coluna3 == linha2_coluna4
                    || linha4_coluna3 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha4_coluna3 == linha3_coluna1
                    || linha4_coluna3 == linha3_coluna2
                    || linha4_coluna3 == linha3_coluna4
                    || linha4_coluna3 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha4_coluna3 == linha5_coluna1
                    || linha4_coluna3 == linha5_coluna2
                    || linha4_coluna3 == linha5_coluna4
                    || linha4_coluna3 == linha5_coluna5
                    || linha4_coluna3 == 0) {
                    linha4_coluna3 += 1;
                }

                if (linha4_coluna4 == linha4_coluna1
                    || linha4_coluna4 == linha4_coluna2
                    || linha4_coluna4 == linha4_coluna3
                    || linha4_coluna4 == linha4_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha4_coluna4 == linha1_coluna1
                    || linha4_coluna4 == linha1_coluna2
                    || linha4_coluna4 == linha1_coluna3
                    || linha4_coluna4 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha4_coluna4 == linha2_coluna1
                    || linha4_coluna4 == linha2_coluna2
                    || linha4_coluna4 == linha2_coluna3
                    || linha4_coluna4 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha4_coluna4 == linha3_coluna1
                    || linha4_coluna4 == linha3_coluna2
                    || linha4_coluna4 == linha3_coluna3
                    || linha4_coluna4 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha4_coluna4 == linha5_coluna1
                    || linha4_coluna4 == linha5_coluna2
                    || linha4_coluna4 == linha5_coluna3
                    || linha4_coluna4 == linha5_coluna5
                    || linha4_coluna4 == 0) {
                    linha4_coluna4 += 1;
                }

                if (linha4_coluna5 == linha4_coluna1
                    || linha4_coluna5 == linha4_coluna2
                    || linha4_coluna5 == linha4_coluna3
                    || linha4_coluna5 == linha4_coluna4
                    //Evitar chances de aparecer na linha 1
                    || linha4_coluna5 == linha1_coluna1
                    || linha4_coluna5 == linha1_coluna2
                    || linha4_coluna5 == linha1_coluna3
                    || linha4_coluna5 == linha1_coluna4
                    //Evitar chances de aparecer na linha 2
                    || linha4_coluna5 == linha2_coluna1
                    || linha4_coluna5 == linha2_coluna2
                    || linha4_coluna5 == linha2_coluna3
                    || linha4_coluna5 == linha2_coluna4
                    //Evitar chances de aparecer na linha 4
                    || linha4_coluna5 == linha3_coluna1
                    || linha4_coluna5 == linha3_coluna2
                    || linha4_coluna5 == linha3_coluna3
                    || linha4_coluna5 == linha3_coluna4
                    //Evitar chances de aparecer na linha 5
                    || linha4_coluna5 == linha5_coluna1
                    || linha4_coluna5 == linha5_coluna2
                    || linha4_coluna5 == linha5_coluna3
                    || linha4_coluna5 == linha5_coluna4
                    || linha4_coluna5 == 0) {
                    linha4_coluna5 += 1;
                }

                /**
                 * Bloco da coluna 4
                 */
                if (
                    linha1_coluna4 == linha2_coluna4
                    || linha1_coluna4 == linha3_coluna4
                    || linha1_coluna4 == linha4_coluna4
                    || linha1_coluna4 == linha5_coluna4
                    || linha1_coluna4 == 0
                ) {
                    linha1_coluna4 += 1;
                }

                if (
                    linha2_coluna4 == linha1_coluna4
                    || linha2_coluna4 == linha3_coluna4
                    || linha2_coluna4 == linha4_coluna4
                    || linha2_coluna4 == linha5_coluna4
                    || linha2_coluna4 == 0
                ) {
                    linha2_coluna4 += 1;
                }

                if (
                    linha3_coluna4 == linha1_coluna4
                    || linha3_coluna4 == linha2_coluna4
                    || linha3_coluna4 == linha4_coluna4
                    || linha3_coluna4 == linha5_coluna4
                    || linha3_coluna4 == 0
                ) {
                    linha3_coluna4 += 1;
                }

                if (
                    linha4_coluna4 == linha1_coluna4
                    || linha4_coluna4 == linha2_coluna4
                    || linha4_coluna4 == linha3_coluna4
                    || linha4_coluna4 == linha5_coluna4
                    || linha4_coluna4 == 0
                ) {
                    linha4_coluna4 += 1;
                }

                if (
                    linha5_coluna4 == linha1_coluna4
                    || linha5_coluna4 == linha2_coluna4
                    || linha5_coluna4 == linha3_coluna4
                    || linha5_coluna4 == linha4_coluna4
                    || linha5_coluna4 == 0
                ) {
                    linha5_coluna4 += 1;
                }

                /**
                * Bloco da linha 5
                */
                if (linha5_coluna1 == linha5_coluna2
                    || linha5_coluna1 == linha5_coluna3
                    || linha5_coluna1 == linha5_coluna4
                    || linha5_coluna1 == linha5_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha5_coluna1 == linha1_coluna2
                    || linha5_coluna1 == linha1_coluna3
                    || linha5_coluna1 == linha1_coluna4
                    || linha5_coluna1 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha5_coluna1 == linha2_coluna2
                    || linha5_coluna1 == linha2_coluna3
                    || linha5_coluna1 == linha2_coluna4
                    || linha5_coluna1 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha5_coluna1 == linha3_coluna2
                    || linha5_coluna1 == linha3_coluna3
                    || linha5_coluna1 == linha3_coluna4
                    || linha5_coluna1 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha5_coluna1 == linha4_coluna2
                    || linha5_coluna1 == linha4_coluna3
                    || linha5_coluna1 == linha4_coluna4
                    || linha5_coluna1 == linha4_coluna5
                    || linha5_coluna1 == 0) {
                    linha5_coluna1 += 1;
                }

                if (linha5_coluna2 == linha5_coluna1
                    || linha5_coluna2 == linha5_coluna3
                    || linha5_coluna2 == linha5_coluna4
                    || linha5_coluna2 == linha5_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha5_coluna2 == linha1_coluna1
                    || linha5_coluna2 == linha1_coluna3
                    || linha5_coluna2 == linha1_coluna4
                    || linha5_coluna2 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha5_coluna2 == linha2_coluna1
                    || linha5_coluna2 == linha2_coluna3
                    || linha5_coluna2 == linha2_coluna4
                    || linha5_coluna2 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha5_coluna2 == linha3_coluna1
                    || linha5_coluna2 == linha3_coluna3
                    || linha5_coluna2 == linha3_coluna4
                    || linha5_coluna2 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha5_coluna2 == linha4_coluna1
                    || linha5_coluna2 == linha4_coluna3
                    || linha5_coluna2 == linha4_coluna4
                    || linha5_coluna2 == linha4_coluna5
                    || linha5_coluna2 == 0) {
                    linha5_coluna2 += 1;
                }

                if (linha5_coluna3 == linha5_coluna1
                    || linha5_coluna3 == linha5_coluna2
                    || linha5_coluna3 == linha5_coluna4
                    || linha5_coluna3 == linha5_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha5_coluna3 == linha1_coluna1
                    || linha5_coluna3 == linha1_coluna2
                    || linha5_coluna3 == linha1_coluna4
                    || linha5_coluna3 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha5_coluna3 == linha2_coluna1
                    || linha5_coluna3 == linha2_coluna2
                    || linha5_coluna3 == linha2_coluna4
                    || linha5_coluna3 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha5_coluna3 == linha3_coluna1
                    || linha5_coluna3 == linha3_coluna2
                    || linha5_coluna3 == linha3_coluna4
                    || linha5_coluna3 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha5_coluna3 == linha4_coluna1
                    || linha5_coluna3 == linha4_coluna2
                    || linha5_coluna3 == linha4_coluna4
                    || linha5_coluna3 == linha4_coluna5
                    || linha5_coluna3 == 0) {
                    linha5_coluna3 += 1;
                }

                if (linha5_coluna4 == linha5_coluna1
                    || linha5_coluna4 == linha5_coluna2
                    || linha5_coluna4 == linha5_coluna3
                    || linha5_coluna4 == linha5_coluna5
                    //Evitar chances de aparecer na linha 1
                    || linha5_coluna4 == linha1_coluna1
                    || linha5_coluna4 == linha1_coluna2
                    || linha5_coluna4 == linha1_coluna3
                    || linha5_coluna4 == linha1_coluna5
                    //Evitar chances de aparecer na linha 2
                    || linha5_coluna4 == linha2_coluna1
                    || linha5_coluna4 == linha2_coluna2
                    || linha5_coluna4 == linha2_coluna3
                    || linha5_coluna4 == linha2_coluna5
                    //Evitar chances de aparecer na linha 4
                    || linha5_coluna4 == linha3_coluna1
                    || linha5_coluna4 == linha3_coluna2
                    || linha5_coluna4 == linha3_coluna3
                    || linha5_coluna4 == linha3_coluna5
                    //Evitar chances de aparecer na linha 5
                    || linha5_coluna4 == linha4_coluna1
                    || linha5_coluna4 == linha4_coluna2
                    || linha5_coluna4 == linha4_coluna3
                    || linha5_coluna4 == linha4_coluna5
                    || linha5_coluna4 == 0) {
                    linha5_coluna4 += 1;
                }

                if (linha5_coluna5 == linha5_coluna1
                    || linha5_coluna5 == linha5_coluna2
                    || linha5_coluna5 == linha5_coluna3
                    || linha5_coluna5 == linha5_coluna4
                    //Evitar chances de aparecer na linha 1
                    || linha5_coluna5 == linha1_coluna1
                    || linha5_coluna5 == linha1_coluna2
                    || linha5_coluna5 == linha1_coluna3
                    || linha5_coluna5 == linha1_coluna4
                    //Evitar chances de aparecer na linha 2
                    || linha5_coluna5 == linha2_coluna1
                    || linha5_coluna5 == linha2_coluna2
                    || linha5_coluna5 == linha2_coluna3
                    || linha5_coluna5 == linha2_coluna4
                    //Evitar chances de aparecer na linha 4
                    || linha5_coluna5 == linha3_coluna1
                    || linha5_coluna5 == linha3_coluna2
                    || linha5_coluna5 == linha3_coluna3
                    || linha5_coluna5 == linha3_coluna4
                    //Evitar chances de aparecer na linha 5
                    || linha5_coluna5 == linha4_coluna1
                    || linha5_coluna5 == linha4_coluna2
                    || linha5_coluna5 == linha4_coluna3
                    || linha5_coluna5 == linha4_coluna4
                    || linha5_coluna5 == 0) {
                    linha5_coluna5 += 1;
                }

                /**
                * Bloco da coluna 5
                */
                if (
                    linha1_coluna5 == linha2_coluna5
                    || linha1_coluna5 == linha3_coluna5
                    || linha1_coluna5 == linha4_coluna5
                    || linha1_coluna5 == linha5_coluna5
                    || linha1_coluna5 == 0
                ) {
                    linha1_coluna5 += 1;
                }

                if (
                    linha2_coluna5 == linha1_coluna5
                    || linha2_coluna5 == linha3_coluna5
                    || linha2_coluna5 == linha4_coluna5
                    || linha2_coluna5 == linha5_coluna5
                    || linha2_coluna5 == 0
                ) {
                    linha2_coluna5 += 1;
                }

                if (
                    linha3_coluna5 == linha1_coluna5
                    || linha3_coluna5 == linha2_coluna5
                    || linha3_coluna5 == linha4_coluna5
                    || linha3_coluna5 == linha5_coluna5
                    || linha3_coluna5 == 0
                ) {
                    linha3_coluna5 += 1;
                }

                if (
                    linha4_coluna5 == linha1_coluna5
                    || linha4_coluna5 == linha2_coluna5
                    || linha4_coluna5 == linha3_coluna5
                    || linha4_coluna5 == linha5_coluna5
                    || linha4_coluna5 == 0
                ) {
                    linha4_coluna5 += 1;
                }

                if (
                    linha5_coluna5 == linha1_coluna5
                    || linha5_coluna5 == linha2_coluna5
                    || linha5_coluna5 == linha3_coluna5
                    || linha5_coluna5 == linha4_coluna5
                    || linha5_coluna5 == 0
                ) {
                    linha5_coluna5 += 1;
                }

                this.cartelas.push(
                    {
                        bingo: {
                            codigo: Math.floor(Math.random() * 1000000 + 1),
                            linha1: {
                                coluna1: linha1_coluna1,
                                coluna2: linha1_coluna2,
                                coluna3: linha1_coluna3,
                                coluna4: linha1_coluna4,
                                coluna5: linha1_coluna5
                            },
                            linha2: {
                                coluna1: linha2_coluna1,
                                coluna2: linha2_coluna2,
                                coluna3: linha2_coluna3,
                                coluna4: linha2_coluna4,
                                coluna5: linha2_coluna5
                            },
                            linha3: {
                                coluna1: linha3_coluna1,
                                coluna2: linha3_coluna2,
                                coluna3: linha3_coluna3,
                                coluna4: linha3_coluna4,
                                coluna5: linha3_coluna5
                            },
                            linha4: {
                                coluna1: linha4_coluna1,
                                coluna2: linha4_coluna2,
                                coluna3: linha4_coluna3,
                                coluna4: linha4_coluna4,
                                coluna5: linha4_coluna5
                            },
                            linha5: {
                                coluna1: linha5_coluna1,
                                coluna2: linha5_coluna2,
                                coluna3: linha5_coluna3,
                                coluna4: linha5_coluna4,
                                coluna5: linha5_coluna5
                            },
                        }
                    })
            }

            let cols = '';

            for (let x = 0; x < this.cartelas.length; x++) {
                cols = cols + '<div class="col-md-4">'
                    + '<h5>Código:' + this.cartelas[x].bingo.codigo + '</h5>'
                    + '<h5>Data: ' + this.dateB + '</h5>'
                    + '<h5>Horário: ' + this.hour + '</h5>'
                    + '<table class="table table-bordered table-striped colortable">'

                    + '<tr>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna1 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna2 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna3 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna4 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna5 + '</h5>'
                    + '</td>'
                    + '</tr>'

                    + '<tr>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna1 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna2 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna3 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna4 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna5 + '</h5>'
                    + '</td>'
                    + '</tr>'

                    + '<tr>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna1 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna2 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna3 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna4 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna5 + '</h5>'
                    + '</td>'
                    + '</tr>'

                    + '<tr>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna1 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna2 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna3 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna4 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna5 + '</h5>'
                    + '</td>'
                    + '</tr>'

                    + '<tr>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna1 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna2 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna3 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna4 + '</h5>'
                    + '</td>'
                    + '<td style="border: solid 3px #d7d7d7">'
                    + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna5 + '</h5>'
                    + '</td>'
                    + '</tr>'
                    + '</table>'
                    + '<h5>Valor: R$ ' + this.value + '</h5>'
                    + '</div>'
            }

            let tables = '<div class="col-md-12">'+ cols +'</div>'

            this.html = '<!DOCTYPE html>'
                + '<html>'
                + '<head>'

                + '<meta charset="utf-8">'
                + '<meta http-equiv="X-UA-Compatible" content="IE=edge">'
                + '<meta name="viewport" content="width=device-width, initial-scale=1">'
                + '<title>Colibri 1.0.0</title>'
                + '<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">'

                + '</head>'

                + '<body id="body">'
                + '<div class="container">'
                + '<div class="row">' + tables + '</div>'
                + '</div>'
                + '</body>'
                + '</html>'

            var fs = require('fs');
            let pathTo = path.join(__dirname)
            fs.writeFile(pathTo + '/bingosGenerated.html', this.html, function (err) {
                if (err) {
                    alert('write pdf file error', err);
                } else {
                    alert('Cartelas geradas com sucesso...');
                    const { BrowserWindow } = require('electron').remote
                    var fs = require('fs');
                    const win = new BrowserWindow({ width: 1024, height: 800 })
                    let pathToPDF = path.join(__dirname)
                    console.log(pathToPDF)
                    win.loadURL(pathToPDF + '/bingosGenerated.html')

                    // win.webContents.on("did-finish-load", function () {
                    //     win.webContents.print({}, function (err, data) {
                    //         if (err) throw error;
                    //     });
                    // });

                    win.webContents.on("did-finish-load", function () {
                        // Use default printing options
                        win.webContents.printToPDF({
                            // printBackground: false,
                            // paperWidth: "12.60",
                            // paperHeight: "7.10",
                            // marginTop: 0,
                            // marginBottom: 0,
                            // marginLeft: 0,
                            // marginRight: 0,
                            // pageRanges: 3
                            // pageSize: "A3"
                        }, function (error, data) {
                            if (error) throw error;
                            let pathWrite = path.join(__dirname)
                            fs.writeFile(pathWrite + '/tmp/output.pdf', data, function (err) {
                                if (err)
                                    alert('write pdf file error', err);
                            })
                        })
                    });


                }
            })
            // console.log(this.cartelas);
            // console.log(html);
        }
    }
});