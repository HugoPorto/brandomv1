window.Vue = require('vue');
const HTML5ToPDF = require("html5-to-pdf")
const path = require("path");

new Vue({
    el: 'html',
    data: {
        file: '',
        html: '',
        dateB: '',
        hour: '',
        value: '',
        locale: '',
        promoter: '',
        bootstrap: false,
        debug: false,
        qtdBingos: 1,
        cartelas: []
    },

    ready: function () {
        var loki = require("lokijs");
        var db = new loki("st.json");
        var read = require('read-file-utf8');
        var data = read('./st.json');
        db.loadJSON(data);

        if (db.getCollection('licenses') == null) {
            Console.log('Software Pirateado...');
            process.exit(0)
        } else {
            console.log('Software funcionando corretamente...');
        }
    },
    methods: {
        html5ToPDF: function () {
            const HTML5ToPDF = require("html5-to-pdf")
            const path = require("path")

            const run = async () => {
                const html5ToPDF = new HTML5ToPDF({
                    inputPath: path.join(__dirname, "bingosGenerated.html"),
                    outputPath: path.join(__dirname, "tmp", "output.pdf"),
                    templatePath: path.join(__dirname),
                    include: [
                        // path.join(__dirname, "templates", "photon.css"),
                        // path.join(__dirname, "bootstrap4/css", "bootstrap.min.css"),
                    ],
                })

                await html5ToPDF.start()
                await html5ToPDF.build()
                await html5ToPDF.close()
                console.log("DONE")
                // alert('Arquivo gerado com sucesso...');
                // process.exit(0)
            }
        },
        makeDebug: function () {
            if (this.debug == false) {
                this.debug = true;
            } else {
                this.debug = false;
            }
        },
        cleanBingos: function () {
            this.file = '';
            this.dateB = '';
            this.hour = '';
            this.value = '';
            this.html = '';
            this.locale = '';
            this.cartelas = [];
            var fs = require('fs');
            let pathToW = path.join(__dirname, '/bingosGenerated.html');
            let pathToWPDF = path.join(__dirname, '/tmp/output.pdf');

            fs.writeFile(pathToW, '', function (err) {
                if (err) {
                    alert('write pdf file error', err);
                }
            })

            fs.writeFile(pathToWPDF, '', function (err) {
                if (err) {
                    alert('write pdf file error', err);
                }
            })
            alert('Tudo limpo...');
        },
        pdfWindow: function () {
            const { BrowserWindow } = require('electron').remote
            const PDFWindow = require('electron-pdf-window')
            const win = new BrowserWindow({ width: 1400, height: 800 })
            PDFWindow.addSupport(win)
            let pathTo = path.join(__dirname, "tmp/output.pdf")

            win.loadURL(pathTo)
        },
        printView: function () {
            const { BrowserWindow } = require('electron').remote
            var fs = require('fs');
            const win = new BrowserWindow({ width: 1024, height: 800 })
            let pathTo = path.join(__dirname)
            console.log(pathTo)
            win.loadURL(pathTo + '/bingosGenerated.html')

            win.webContents.on("did-finish-load", function () {
                win.webContents.print({}, function (err, data) {
                    if (err) throw error;
                });
            });
        },
        viewNormal: function () {
            const { BrowserWindow } = require('electron').remote
            var fs = require('fs');
            const win = new BrowserWindow({ width: 1024, height: 800 })
            let pathTo = path.join(__dirname, '/bingosGenerated.html')
            win.loadURL(pathTo)
        },
        generateBingos: function () {
            let file = '';
            let cols = '';
            let qtdHTMLs = this.qtdBingos / 4;
            for (countHTMLs = 0; countHTMLs < qtdHTMLs; countHTMLs++) {
                this.cartelas = [];
                for (var qtd = 1; qtd <= 4; qtd++) {
                    /**
                     * Bloco de definição da linha 1
                     */
                    let linha1_coluna1 = Math.floor(Math.random() * 74 + 1);
                    let linha1_coluna2 = Math.floor(Math.random() * 74 + 1);
                    let linha1_coluna3 = Math.floor(Math.random() * 74 + 1);
                    let linha1_coluna4 = Math.floor(Math.random() * 74 + 1);
                    let linha1_coluna5 = Math.floor(Math.random() * 74 + 1);

                    /**
                     * Bloco de definição da linha 2
                     */
                    let linha2_coluna1 = Math.floor(Math.random() * 74 + 1);
                    let linha2_coluna2 = Math.floor(Math.random() * 74 + 1);
                    let linha2_coluna3 = Math.floor(Math.random() * 74 + 1);
                    let linha2_coluna4 = Math.floor(Math.random() * 74 + 1);
                    let linha2_coluna5 = Math.floor(Math.random() * 74 + 1);

                    /**
                     * Bloco de definição da linha 3
                     */
                    let linha3_coluna1 = Math.floor(Math.random() * 74 + 1);
                    let linha3_coluna2 = Math.floor(Math.random() * 74 + 1);
                    let linha3_coluna3 = Math.floor(Math.random() * 74 + 1);
                    let linha3_coluna4 = Math.floor(Math.random() * 74 + 1);
                    let linha3_coluna5 = Math.floor(Math.random() * 74 + 1);

                    /**
                     * Bloco de definição da linha 4
                     */
                    let linha4_coluna1 = Math.floor(Math.random() * 74 + 1);
                    let linha4_coluna2 = Math.floor(Math.random() * 74 + 1);
                    let linha4_coluna3 = Math.floor(Math.random() * 74 + 1);
                    let linha4_coluna4 = Math.floor(Math.random() * 74 + 1);
                    let linha4_coluna5 = Math.floor(Math.random() * 74 + 1);

                    /**
                     * Bloco de definição da linha 5
                     */
                    let linha5_coluna1 = Math.floor(Math.random() * 74 + 1);
                    let linha5_coluna2 = Math.floor(Math.random() * 74 + 1);
                    let linha5_coluna3 = Math.floor(Math.random() * 74 + 1);
                    let linha5_coluna4 = Math.floor(Math.random() * 74 + 1);
                    let linha5_coluna5 = Math.floor(Math.random() * 74 + 1);

                    /**
                     * Bloco da linha 1
                     */
                    if (
                        linha1_coluna1 == linha1_coluna2
                        || linha1_coluna1 == linha1_coluna3
                        || linha1_coluna1 == linha1_coluna4
                        || linha1_coluna1 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha1_coluna1 == linha2_coluna2
                        || linha1_coluna1 == linha2_coluna3
                        || linha1_coluna1 == linha2_coluna4
                        || linha1_coluna1 == linha2_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha1_coluna1 == linha3_coluna2
                        || linha1_coluna1 == linha3_coluna3
                        || linha1_coluna1 == linha3_coluna4
                        || linha1_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha1_coluna1 == linha4_coluna2
                        || linha1_coluna1 == linha4_coluna3
                        || linha1_coluna1 == linha4_coluna4
                        || linha1_coluna1 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha1_coluna1 == linha5_coluna2
                        || linha1_coluna1 == linha5_coluna3
                        || linha1_coluna1 == linha5_coluna4
                        || linha1_coluna1 == linha5_coluna5
                        || linha1_coluna1 == 0
                    ) {
                        linha1_coluna1 += 1;
                    }

                    if (linha1_coluna2 == linha1_coluna1
                        || linha1_coluna2 == linha1_coluna3
                        || linha1_coluna2 == linha1_coluna4
                        || linha1_coluna2 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha1_coluna2 == linha2_coluna1
                        || linha1_coluna2 == linha2_coluna3
                        || linha1_coluna2 == linha2_coluna4
                        || linha1_coluna2 == linha2_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha1_coluna2 == linha3_coluna1
                        || linha1_coluna2 == linha3_coluna3
                        || linha1_coluna2 == linha3_coluna4
                        || linha1_coluna2 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha1_coluna2 == linha4_coluna1
                        || linha1_coluna2 == linha4_coluna3
                        || linha1_coluna2 == linha4_coluna4
                        || linha1_coluna2 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha1_coluna2 == linha5_coluna1
                        || linha1_coluna2 == linha5_coluna3
                        || linha1_coluna2 == linha5_coluna4
                        || linha1_coluna2 == linha5_coluna5
                        || linha1_coluna2 == 0) {
                        linha1_coluna2 += 1;
                    }

                    if (linha1_coluna3 == linha1_coluna1
                        || linha1_coluna3 == linha1_coluna2
                        || linha1_coluna3 == linha1_coluna4
                        || linha1_coluna3 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha1_coluna3 == linha2_coluna1
                        || linha1_coluna3 == linha2_coluna2
                        || linha1_coluna3 == linha2_coluna4
                        || linha1_coluna3 == linha2_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha1_coluna3 == linha3_coluna1
                        || linha1_coluna3 == linha3_coluna2
                        || linha1_coluna3 == linha3_coluna4
                        || linha1_coluna3 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha1_coluna3 == linha4_coluna1
                        || linha1_coluna3 == linha4_coluna2
                        || linha1_coluna3 == linha4_coluna4
                        || linha1_coluna3 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha1_coluna3 == linha5_coluna1
                        || linha1_coluna3 == linha5_coluna2
                        || linha1_coluna3 == linha5_coluna4
                        || linha1_coluna3 == linha5_coluna5
                        || linha1_coluna3 == 0) {
                        linha1_coluna3 += 1;
                    }

                    if (linha1_coluna4 == linha1_coluna1
                        || linha1_coluna4 == linha1_coluna2
                        || linha1_coluna4 == linha1_coluna3
                        || linha1_coluna4 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha1_coluna4 == linha2_coluna1
                        || linha1_coluna4 == linha2_coluna2
                        || linha1_coluna4 == linha2_coluna3
                        || linha1_coluna4 == linha2_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha1_coluna4 == linha3_coluna1
                        || linha1_coluna4 == linha3_coluna2
                        || linha1_coluna4 == linha3_coluna3
                        || linha1_coluna4 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha1_coluna4 == linha4_coluna1
                        || linha1_coluna4 == linha4_coluna2
                        || linha1_coluna4 == linha4_coluna3
                        || linha1_coluna4 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha1_coluna4 == linha5_coluna1
                        || linha1_coluna4 == linha5_coluna2
                        || linha1_coluna4 == linha5_coluna3
                        || linha1_coluna4 == linha5_coluna5
                        || linha1_coluna4 == 0) {
                        linha1_coluna4 += 1;
                    }

                    if (linha1_coluna5 == linha1_coluna1
                        || linha1_coluna5 == linha1_coluna2
                        || linha1_coluna5 == linha1_coluna3
                        || linha1_coluna5 == linha1_coluna4
                        //Evitar chances de aparecer na linha 2
                        || linha1_coluna5 == linha2_coluna1
                        || linha1_coluna5 == linha2_coluna2
                        || linha1_coluna5 == linha2_coluna3
                        || linha1_coluna5 == linha2_coluna4
                        //Evitar chances de aparecer na linha 3
                        || linha1_coluna5 == linha3_coluna1
                        || linha1_coluna5 == linha3_coluna2
                        || linha1_coluna5 == linha3_coluna3
                        || linha1_coluna5 == linha3_coluna4
                        //Evitar chances de aparecer na linha 4
                        || linha1_coluna5 == linha4_coluna1
                        || linha1_coluna5 == linha4_coluna2
                        || linha1_coluna5 == linha4_coluna3
                        || linha1_coluna5 == linha4_coluna4
                        //Evitar chances de aparecer na linha 5
                        || linha1_coluna5 == linha5_coluna1
                        || linha1_coluna5 == linha5_coluna2
                        || linha1_coluna5 == linha5_coluna3
                        || linha1_coluna5 == linha5_coluna4
                        || linha1_coluna5 == 0) {
                        linha1_coluna5 += 1;
                    }

                    /**
                     * Bloco da coluna 1
                     */
                    if (
                        linha1_coluna1 == linha2_coluna1
                        || linha1_coluna1 == linha3_coluna1
                        || linha1_coluna1 == linha4_coluna1
                        || linha1_coluna1 == linha5_coluna1
                        || linha1_coluna1 == 0
                    ) {
                        linha1_coluna1 += 1;
                    }

                    if (
                        linha2_coluna1 == linha1_coluna1
                        || linha2_coluna1 == linha3_coluna1
                        || linha2_coluna1 == linha4_coluna1
                        || linha2_coluna1 == linha5_coluna1
                        || linha2_coluna1 == 0
                    ) {
                        linha2_coluna1 += 1;
                    }

                    if (
                        linha3_coluna1 == linha1_coluna1
                        || linha3_coluna1 == linha2_coluna1
                        || linha3_coluna1 == linha4_coluna1
                        || linha3_coluna1 == linha5_coluna1
                        || linha3_coluna1 == 0
                    ) {
                        linha3_coluna1 += 1;
                    }

                    if (
                        linha4_coluna1 == linha1_coluna1
                        || linha4_coluna1 == linha2_coluna1
                        || linha4_coluna1 == linha3_coluna1
                        || linha4_coluna1 == linha5_coluna1
                        || linha4_coluna1 == 0
                    ) {
                        linha4_coluna1 += 1;
                    }

                    if (
                        linha5_coluna1 == linha1_coluna1
                        || linha5_coluna1 == linha2_coluna1
                        || linha5_coluna1 == linha3_coluna1
                        || linha5_coluna1 == linha4_coluna1
                        || linha5_coluna1 == 0
                    ) {
                        linha5_coluna1 += 1;
                    }

                    /**
                    * Bloco da linha 2
                    */
                    if (linha2_coluna1 == linha2_coluna2
                        || linha2_coluna1 == linha2_coluna3
                        || linha2_coluna1 == linha2_coluna4
                        || linha2_coluna1 == linha2_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha2_coluna1 == linha1_coluna2
                        || linha2_coluna1 == linha1_coluna3
                        || linha2_coluna1 == linha1_coluna4
                        || linha2_coluna1 == linha1_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha2_coluna1 == linha3_coluna2
                        || linha2_coluna1 == linha3_coluna3
                        || linha2_coluna1 == linha3_coluna4
                        || linha2_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha2_coluna1 == linha4_coluna2
                        || linha2_coluna1 == linha4_coluna3
                        || linha2_coluna1 == linha4_coluna4
                        || linha2_coluna1 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha2_coluna1 == linha5_coluna2
                        || linha2_coluna1 == linha5_coluna3
                        || linha2_coluna1 == linha5_coluna4
                        || linha2_coluna1 == linha5_coluna5
                        || linha2_coluna1 == 0) {
                        linha2_coluna1 += 1;
                    }

                    if (linha2_coluna2 == linha2_coluna1
                        || linha2_coluna2 == linha2_coluna3
                        || linha2_coluna2 == linha2_coluna4
                        || linha2_coluna2 == linha2_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha2_coluna2 == linha1_coluna1
                        || linha2_coluna2 == linha1_coluna3
                        || linha2_coluna2 == linha1_coluna4
                        || linha2_coluna2 == linha1_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha2_coluna2 == linha3_coluna1
                        || linha2_coluna2 == linha3_coluna3
                        || linha2_coluna2 == linha3_coluna4
                        || linha2_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha2_coluna2 == linha4_coluna1
                        || linha2_coluna2 == linha4_coluna3
                        || linha2_coluna2 == linha4_coluna4
                        || linha2_coluna2 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha2_coluna2 == linha5_coluna1
                        || linha2_coluna2 == linha5_coluna3
                        || linha2_coluna2 == linha5_coluna4
                        || linha2_coluna2 == linha5_coluna5
                        || linha2_coluna2 == 0) {
                        linha2_coluna2 += 1;
                    }

                    if (linha2_coluna3 == linha2_coluna1
                        || linha2_coluna3 == linha2_coluna2
                        || linha2_coluna3 == linha2_coluna4
                        || linha2_coluna3 == linha2_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha2_coluna3 == linha1_coluna1
                        || linha2_coluna3 == linha1_coluna2
                        || linha2_coluna3 == linha1_coluna4
                        || linha2_coluna3 == linha1_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha2_coluna3 == linha3_coluna1
                        || linha2_coluna3 == linha3_coluna2
                        || linha2_coluna3 == linha3_coluna4
                        || linha2_coluna3 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha2_coluna3 == linha4_coluna1
                        || linha2_coluna3 == linha4_coluna2
                        || linha2_coluna3 == linha4_coluna4
                        || linha2_coluna3 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha2_coluna3 == linha5_coluna1
                        || linha2_coluna3 == linha5_coluna2
                        || linha2_coluna3 == linha5_coluna4
                        || linha2_coluna3 == linha5_coluna5
                        || linha2_coluna3 == 0) {
                        linha2_coluna3 += 1;
                    }

                    if (linha2_coluna4 == linha2_coluna1
                        || linha2_coluna4 == linha2_coluna2
                        || linha2_coluna4 == linha2_coluna3
                        || linha2_coluna4 == linha2_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha2_coluna4 == linha1_coluna1
                        || linha2_coluna4 == linha1_coluna2
                        || linha2_coluna4 == linha1_coluna3
                        || linha2_coluna4 == linha1_coluna5
                        //Evitar chances de aparecer na linha 3
                        || linha2_coluna4 == linha3_coluna1
                        || linha2_coluna4 == linha3_coluna2
                        || linha2_coluna4 == linha3_coluna3
                        || linha2_coluna4 == linha3_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha2_coluna4 == linha4_coluna1
                        || linha2_coluna4 == linha4_coluna2
                        || linha2_coluna4 == linha4_coluna3
                        || linha2_coluna4 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha2_coluna4 == linha5_coluna1
                        || linha2_coluna4 == linha5_coluna2
                        || linha2_coluna4 == linha5_coluna3
                        || linha2_coluna4 == linha5_coluna5
                        || linha2_coluna4 == 0) {
                        linha2_coluna4 += 1;
                    }

                    if (linha2_coluna5 == linha2_coluna1
                        || linha2_coluna5 == linha2_coluna2
                        || linha2_coluna5 == linha2_coluna3
                        || linha2_coluna5 == linha2_coluna4
                        //Evitar chances de aparecer na linha 1
                        || linha2_coluna5 == linha1_coluna1
                        || linha2_coluna5 == linha1_coluna2
                        || linha2_coluna5 == linha1_coluna3
                        || linha2_coluna5 == linha1_coluna4
                        //Evitar chances de aparecer na linha 3
                        || linha2_coluna5 == linha3_coluna1
                        || linha2_coluna5 == linha3_coluna2
                        || linha2_coluna5 == linha3_coluna3
                        || linha2_coluna5 == linha3_coluna4
                        //Evitar chances de aparecer na linha 4
                        || linha2_coluna5 == linha4_coluna1
                        || linha2_coluna5 == linha4_coluna2
                        || linha2_coluna5 == linha4_coluna3
                        || linha2_coluna5 == linha4_coluna4
                        //Evitar chances de aparecer na linha 5
                        || linha2_coluna5 == linha5_coluna1
                        || linha2_coluna5 == linha5_coluna2
                        || linha2_coluna5 == linha5_coluna3
                        || linha2_coluna5 == linha5_coluna4
                        || linha2_coluna5 == 0) {
                        linha2_coluna5 += 1;
                    }

                    /**
                    * Bloco da coluna 2
                    */
                    if (
                        linha1_coluna2 == linha2_coluna2
                        || linha1_coluna2 == linha3_coluna2
                        || linha1_coluna2 == linha4_coluna2
                        || linha1_coluna2 == linha5_coluna2
                        || linha1_coluna2 == 0
                    ) {
                        linha1_coluna2 += 1;
                    }

                    if (
                        linha2_coluna2 == linha1_coluna2
                        || linha2_coluna2 == linha3_coluna2
                        || linha2_coluna2 == linha4_coluna2
                        || linha2_coluna2 == linha5_coluna2
                        || linha2_coluna2 == 0
                    ) {
                        linha2_coluna2 += 1;
                    }

                    if (
                        linha3_coluna2 == linha1_coluna2
                        || linha3_coluna2 == linha2_coluna2
                        || linha3_coluna2 == linha4_coluna2
                        || linha3_coluna2 == linha5_coluna2
                        || linha3_coluna2 == 0
                    ) {
                        linha3_coluna2 += 1;
                    }

                    if (
                        linha4_coluna2 == linha1_coluna2
                        || linha4_coluna2 == linha2_coluna2
                        || linha4_coluna2 == linha3_coluna2
                        || linha4_coluna2 == linha5_coluna2
                        || linha4_coluna2 == 0
                    ) {
                        linha4_coluna2 += 1;
                    }

                    if (
                        linha5_coluna2 == linha1_coluna2
                        || linha5_coluna2 == linha2_coluna2
                        || linha5_coluna2 == linha3_coluna2
                        || linha5_coluna2 == linha4_coluna2
                        || linha5_coluna2 == 0
                    ) {
                        linha5_coluna2 += 1;
                    }

                    /**
                    * Bloco da linha 3
                    */
                    if (linha3_coluna1 == linha3_coluna2
                        || linha3_coluna1 == linha3_coluna3
                        || linha3_coluna1 == linha3_coluna4
                        || linha3_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha3_coluna1 == linha1_coluna2
                        || linha3_coluna1 == linha1_coluna3
                        || linha3_coluna1 == linha1_coluna4
                        || linha3_coluna1 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha3_coluna1 == linha2_coluna2
                        || linha3_coluna1 == linha2_coluna3
                        || linha3_coluna1 == linha2_coluna4
                        || linha3_coluna1 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha3_coluna1 == linha4_coluna2
                        || linha3_coluna1 == linha4_coluna3
                        || linha3_coluna1 == linha4_coluna4
                        || linha3_coluna1 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha3_coluna1 == linha5_coluna2
                        || linha3_coluna1 == linha5_coluna3
                        || linha3_coluna1 == linha5_coluna4
                        || linha3_coluna1 == linha5_coluna5
                        || linha3_coluna1 == 0) {
                        linha3_coluna1 += 1;
                    }

                    if (linha3_coluna2 == linha3_coluna1
                        || linha3_coluna2 == linha3_coluna3
                        || linha3_coluna2 == linha3_coluna4
                        || linha3_coluna2 == linha3_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha3_coluna2 == linha1_coluna1
                        || linha3_coluna2 == linha1_coluna3
                        || linha3_coluna2 == linha1_coluna4
                        || linha3_coluna2 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha3_coluna2 == linha2_coluna1
                        || linha3_coluna2 == linha2_coluna3
                        || linha3_coluna2 == linha2_coluna4
                        || linha3_coluna2 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha3_coluna2 == linha4_coluna1
                        || linha3_coluna2 == linha4_coluna3
                        || linha3_coluna2 == linha4_coluna4
                        || linha3_coluna2 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha3_coluna2 == linha5_coluna1
                        || linha3_coluna2 == linha5_coluna3
                        || linha3_coluna2 == linha5_coluna4
                        || linha3_coluna2 == linha5_coluna5
                        || linha3_coluna2 == 0) {
                        linha3_coluna2 += 1;
                    }

                    if (linha3_coluna3 == linha3_coluna1
                        || linha3_coluna3 == linha3_coluna2
                        || linha3_coluna3 == linha3_coluna4
                        || linha3_coluna3 == linha3_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha3_coluna3 == linha1_coluna1
                        || linha3_coluna3 == linha1_coluna2
                        || linha3_coluna3 == linha1_coluna4
                        || linha3_coluna3 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha3_coluna3 == linha2_coluna1
                        || linha3_coluna3 == linha2_coluna2
                        || linha3_coluna3 == linha2_coluna4
                        || linha3_coluna3 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha3_coluna3 == linha4_coluna1
                        || linha3_coluna3 == linha4_coluna2
                        || linha3_coluna3 == linha4_coluna4
                        || linha3_coluna3 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha3_coluna3 == linha5_coluna1
                        || linha3_coluna3 == linha5_coluna2
                        || linha3_coluna3 == linha5_coluna4
                        || linha3_coluna3 == linha5_coluna5
                        || linha3_coluna3 == 0) {
                        linha3_coluna3 += 1;
                    }

                    if (linha3_coluna4 == linha3_coluna1
                        || linha3_coluna4 == linha3_coluna2
                        || linha3_coluna4 == linha3_coluna3
                        || linha3_coluna4 == linha3_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha3_coluna4 == linha1_coluna1
                        || linha3_coluna4 == linha1_coluna2
                        || linha3_coluna4 == linha1_coluna3
                        || linha3_coluna4 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha3_coluna4 == linha2_coluna1
                        || linha3_coluna4 == linha2_coluna2
                        || linha3_coluna4 == linha2_coluna3
                        || linha3_coluna4 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha3_coluna4 == linha4_coluna1
                        || linha3_coluna4 == linha4_coluna2
                        || linha3_coluna4 == linha4_coluna3
                        || linha3_coluna4 == linha4_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha3_coluna4 == linha5_coluna1
                        || linha3_coluna4 == linha5_coluna2
                        || linha3_coluna4 == linha5_coluna3
                        || linha3_coluna4 == linha5_coluna5
                        || linha3_coluna4 == 0) {
                        linha3_coluna4 += 1;
                    }

                    if (linha3_coluna5 == linha3_coluna1
                        || linha3_coluna5 == linha3_coluna2
                        || linha3_coluna5 == linha3_coluna3
                        || linha3_coluna5 == linha3_coluna4
                        //Evitar chances de aparecer na linha 1
                        || linha3_coluna5 == linha1_coluna1
                        || linha3_coluna5 == linha1_coluna2
                        || linha3_coluna5 == linha1_coluna3
                        || linha3_coluna5 == linha1_coluna4
                        //Evitar chances de aparecer na linha 2
                        || linha3_coluna5 == linha2_coluna1
                        || linha3_coluna5 == linha2_coluna2
                        || linha3_coluna5 == linha2_coluna3
                        || linha3_coluna5 == linha2_coluna4
                        //Evitar chances de aparecer na linha 4
                        || linha3_coluna5 == linha4_coluna1
                        || linha3_coluna5 == linha4_coluna2
                        || linha3_coluna5 == linha4_coluna3
                        || linha3_coluna5 == linha4_coluna4
                        //Evitar chances de aparecer na linha 5
                        || linha3_coluna5 == linha5_coluna1
                        || linha3_coluna5 == linha5_coluna2
                        || linha3_coluna5 == linha5_coluna3
                        || linha3_coluna5 == linha5_coluna4
                        || linha3_coluna5 == 0) {
                        linha3_coluna5 += 1;
                    }

                    /**
                   * Bloco da coluna 3
                   */
                    if (
                        linha1_coluna3 == linha2_coluna3
                        || linha1_coluna3 == linha3_coluna3
                        || linha1_coluna3 == linha4_coluna3
                        || linha1_coluna3 == linha5_coluna3
                        || linha1_coluna3 == 0
                    ) {
                        linha1_coluna3 += 1;
                    }

                    if (
                        linha2_coluna3 == linha1_coluna3
                        || linha2_coluna3 == linha3_coluna3
                        || linha2_coluna3 == linha4_coluna3
                        || linha2_coluna3 == linha5_coluna3
                        || linha2_coluna3 == 0
                    ) {
                        linha2_coluna3 += 1;
                    }

                    if (
                        linha3_coluna3 == linha1_coluna3
                        || linha3_coluna3 == linha2_coluna3
                        || linha3_coluna3 == linha4_coluna3
                        || linha3_coluna3 == linha5_coluna3
                        || linha3_coluna3 == 0
                    ) {
                        linha3_coluna3 += 1;
                    }

                    if (
                        linha4_coluna3 == linha1_coluna3
                        || linha4_coluna3 == linha2_coluna3
                        || linha4_coluna3 == linha3_coluna3
                        || linha4_coluna3 == linha5_coluna3
                        || linha4_coluna3 == 0
                    ) {
                        linha4_coluna3 += 1;
                    }

                    if (
                        linha5_coluna3 == linha1_coluna3
                        || linha5_coluna3 == linha2_coluna3
                        || linha5_coluna3 == linha3_coluna3
                        || linha5_coluna3 == linha4_coluna3
                        || linha5_coluna3 == 0
                    ) {
                        linha5_coluna3 += 1;
                    }

                    /**
                    * Bloco da linha 4
                    */
                    if (linha4_coluna1 == linha4_coluna2
                        || linha4_coluna1 == linha4_coluna3
                        || linha4_coluna1 == linha4_coluna4
                        || linha4_coluna1 == linha4_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha4_coluna1 == linha1_coluna2
                        || linha4_coluna1 == linha1_coluna3
                        || linha4_coluna1 == linha1_coluna4
                        || linha4_coluna1 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha4_coluna1 == linha2_coluna2
                        || linha4_coluna1 == linha2_coluna3
                        || linha4_coluna1 == linha2_coluna4
                        || linha4_coluna1 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha4_coluna1 == linha3_coluna2
                        || linha4_coluna1 == linha3_coluna3
                        || linha4_coluna1 == linha3_coluna4
                        || linha4_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha4_coluna1 == linha5_coluna2
                        || linha4_coluna1 == linha5_coluna3
                        || linha4_coluna1 == linha5_coluna4
                        || linha4_coluna1 == linha5_coluna5
                        || linha4_coluna1 == 0) {
                        linha4_coluna1 += 1;
                    }

                    if (linha4_coluna2 == linha4_coluna1
                        || linha4_coluna2 == linha4_coluna3
                        || linha4_coluna2 == linha4_coluna4
                        || linha4_coluna2 == linha4_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha4_coluna2 == linha1_coluna1
                        || linha4_coluna2 == linha1_coluna3
                        || linha4_coluna2 == linha1_coluna4
                        || linha4_coluna2 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha4_coluna2 == linha2_coluna1
                        || linha4_coluna2 == linha2_coluna3
                        || linha4_coluna2 == linha2_coluna4
                        || linha4_coluna2 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha4_coluna2 == linha3_coluna1
                        || linha4_coluna2 == linha3_coluna3
                        || linha4_coluna2 == linha3_coluna4
                        || linha4_coluna2 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha4_coluna2 == linha5_coluna1
                        || linha4_coluna2 == linha5_coluna3
                        || linha4_coluna2 == linha5_coluna4
                        || linha4_coluna2 == linha5_coluna5
                        || linha4_coluna2 == 0) {
                        linha4_coluna2 += 1;
                    }

                    if (linha4_coluna3 == linha4_coluna1
                        || linha4_coluna3 == linha4_coluna2
                        || linha4_coluna3 == linha4_coluna4
                        || linha4_coluna3 == linha4_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha4_coluna3 == linha1_coluna1
                        || linha4_coluna3 == linha1_coluna2
                        || linha4_coluna3 == linha1_coluna4
                        || linha4_coluna3 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha4_coluna3 == linha2_coluna1
                        || linha4_coluna3 == linha2_coluna2
                        || linha4_coluna3 == linha2_coluna4
                        || linha4_coluna3 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha4_coluna3 == linha3_coluna1
                        || linha4_coluna3 == linha3_coluna2
                        || linha4_coluna3 == linha3_coluna4
                        || linha4_coluna3 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha4_coluna3 == linha5_coluna1
                        || linha4_coluna3 == linha5_coluna2
                        || linha4_coluna3 == linha5_coluna4
                        || linha4_coluna3 == linha5_coluna5
                        || linha4_coluna3 == 0) {
                        linha4_coluna3 += 1;
                    }

                    if (linha4_coluna4 == linha4_coluna1
                        || linha4_coluna4 == linha4_coluna2
                        || linha4_coluna4 == linha4_coluna3
                        || linha4_coluna4 == linha4_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha4_coluna4 == linha1_coluna1
                        || linha4_coluna4 == linha1_coluna2
                        || linha4_coluna4 == linha1_coluna3
                        || linha4_coluna4 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha4_coluna4 == linha2_coluna1
                        || linha4_coluna4 == linha2_coluna2
                        || linha4_coluna4 == linha2_coluna3
                        || linha4_coluna4 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha4_coluna4 == linha3_coluna1
                        || linha4_coluna4 == linha3_coluna2
                        || linha4_coluna4 == linha3_coluna3
                        || linha4_coluna4 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha4_coluna4 == linha5_coluna1
                        || linha4_coluna4 == linha5_coluna2
                        || linha4_coluna4 == linha5_coluna3
                        || linha4_coluna4 == linha5_coluna5
                        || linha4_coluna4 == 0) {
                        linha4_coluna4 += 1;
                    }

                    if (linha4_coluna5 == linha4_coluna1
                        || linha4_coluna5 == linha4_coluna2
                        || linha4_coluna5 == linha4_coluna3
                        || linha4_coluna5 == linha4_coluna4
                        //Evitar chances de aparecer na linha 1
                        || linha4_coluna5 == linha1_coluna1
                        || linha4_coluna5 == linha1_coluna2
                        || linha4_coluna5 == linha1_coluna3
                        || linha4_coluna5 == linha1_coluna4
                        //Evitar chances de aparecer na linha 2
                        || linha4_coluna5 == linha2_coluna1
                        || linha4_coluna5 == linha2_coluna2
                        || linha4_coluna5 == linha2_coluna3
                        || linha4_coluna5 == linha2_coluna4
                        //Evitar chances de aparecer na linha 4
                        || linha4_coluna5 == linha3_coluna1
                        || linha4_coluna5 == linha3_coluna2
                        || linha4_coluna5 == linha3_coluna3
                        || linha4_coluna5 == linha3_coluna4
                        //Evitar chances de aparecer na linha 5
                        || linha4_coluna5 == linha5_coluna1
                        || linha4_coluna5 == linha5_coluna2
                        || linha4_coluna5 == linha5_coluna3
                        || linha4_coluna5 == linha5_coluna4
                        || linha4_coluna5 == 0) {
                        linha4_coluna5 += 1;
                    }

                    /**
                     * Bloco da coluna 4
                     */
                    if (
                        linha1_coluna4 == linha2_coluna4
                        || linha1_coluna4 == linha3_coluna4
                        || linha1_coluna4 == linha4_coluna4
                        || linha1_coluna4 == linha5_coluna4
                        || linha1_coluna4 == 0
                    ) {
                        linha1_coluna4 += 1;
                    }

                    if (
                        linha2_coluna4 == linha1_coluna4
                        || linha2_coluna4 == linha3_coluna4
                        || linha2_coluna4 == linha4_coluna4
                        || linha2_coluna4 == linha5_coluna4
                        || linha2_coluna4 == 0
                    ) {
                        linha2_coluna4 += 1;
                    }

                    if (
                        linha3_coluna4 == linha1_coluna4
                        || linha3_coluna4 == linha2_coluna4
                        || linha3_coluna4 == linha4_coluna4
                        || linha3_coluna4 == linha5_coluna4
                        || linha3_coluna4 == 0
                    ) {
                        linha3_coluna4 += 1;
                    }

                    if (
                        linha4_coluna4 == linha1_coluna4
                        || linha4_coluna4 == linha2_coluna4
                        || linha4_coluna4 == linha3_coluna4
                        || linha4_coluna4 == linha5_coluna4
                        || linha4_coluna4 == 0
                    ) {
                        linha4_coluna4 += 1;
                    }

                    if (
                        linha5_coluna4 == linha1_coluna4
                        || linha5_coluna4 == linha2_coluna4
                        || linha5_coluna4 == linha3_coluna4
                        || linha5_coluna4 == linha4_coluna4
                        || linha5_coluna4 == 0
                    ) {
                        linha5_coluna4 += 1;
                    }

                    /**
                    * Bloco da linha 5
                    */
                    if (linha5_coluna1 == linha5_coluna2
                        || linha5_coluna1 == linha5_coluna3
                        || linha5_coluna1 == linha5_coluna4
                        || linha5_coluna1 == linha5_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha5_coluna1 == linha1_coluna2
                        || linha5_coluna1 == linha1_coluna3
                        || linha5_coluna1 == linha1_coluna4
                        || linha5_coluna1 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha5_coluna1 == linha2_coluna2
                        || linha5_coluna1 == linha2_coluna3
                        || linha5_coluna1 == linha2_coluna4
                        || linha5_coluna1 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha5_coluna1 == linha3_coluna2
                        || linha5_coluna1 == linha3_coluna3
                        || linha5_coluna1 == linha3_coluna4
                        || linha5_coluna1 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha5_coluna1 == linha4_coluna2
                        || linha5_coluna1 == linha4_coluna3
                        || linha5_coluna1 == linha4_coluna4
                        || linha5_coluna1 == linha4_coluna5
                        || linha5_coluna1 == 0) {
                        linha5_coluna1 += 1;
                    }

                    if (linha5_coluna2 == linha5_coluna1
                        || linha5_coluna2 == linha5_coluna3
                        || linha5_coluna2 == linha5_coluna4
                        || linha5_coluna2 == linha5_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha5_coluna2 == linha1_coluna1
                        || linha5_coluna2 == linha1_coluna3
                        || linha5_coluna2 == linha1_coluna4
                        || linha5_coluna2 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha5_coluna2 == linha2_coluna1
                        || linha5_coluna2 == linha2_coluna3
                        || linha5_coluna2 == linha2_coluna4
                        || linha5_coluna2 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha5_coluna2 == linha3_coluna1
                        || linha5_coluna2 == linha3_coluna3
                        || linha5_coluna2 == linha3_coluna4
                        || linha5_coluna2 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha5_coluna2 == linha4_coluna1
                        || linha5_coluna2 == linha4_coluna3
                        || linha5_coluna2 == linha4_coluna4
                        || linha5_coluna2 == linha4_coluna5
                        || linha5_coluna2 == 0) {
                        linha5_coluna2 += 1;
                    }

                    if (linha5_coluna3 == linha5_coluna1
                        || linha5_coluna3 == linha5_coluna2
                        || linha5_coluna3 == linha5_coluna4
                        || linha5_coluna3 == linha5_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha5_coluna3 == linha1_coluna1
                        || linha5_coluna3 == linha1_coluna2
                        || linha5_coluna3 == linha1_coluna4
                        || linha5_coluna3 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha5_coluna3 == linha2_coluna1
                        || linha5_coluna3 == linha2_coluna2
                        || linha5_coluna3 == linha2_coluna4
                        || linha5_coluna3 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha5_coluna3 == linha3_coluna1
                        || linha5_coluna3 == linha3_coluna2
                        || linha5_coluna3 == linha3_coluna4
                        || linha5_coluna3 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha5_coluna3 == linha4_coluna1
                        || linha5_coluna3 == linha4_coluna2
                        || linha5_coluna3 == linha4_coluna4
                        || linha5_coluna3 == linha4_coluna5
                        || linha5_coluna3 == 0) {
                        linha5_coluna3 += 1;
                    }

                    if (linha5_coluna4 == linha5_coluna1
                        || linha5_coluna4 == linha5_coluna2
                        || linha5_coluna4 == linha5_coluna3
                        || linha5_coluna4 == linha5_coluna5
                        //Evitar chances de aparecer na linha 1
                        || linha5_coluna4 == linha1_coluna1
                        || linha5_coluna4 == linha1_coluna2
                        || linha5_coluna4 == linha1_coluna3
                        || linha5_coluna4 == linha1_coluna5
                        //Evitar chances de aparecer na linha 2
                        || linha5_coluna4 == linha2_coluna1
                        || linha5_coluna4 == linha2_coluna2
                        || linha5_coluna4 == linha2_coluna3
                        || linha5_coluna4 == linha2_coluna5
                        //Evitar chances de aparecer na linha 4
                        || linha5_coluna4 == linha3_coluna1
                        || linha5_coluna4 == linha3_coluna2
                        || linha5_coluna4 == linha3_coluna3
                        || linha5_coluna4 == linha3_coluna5
                        //Evitar chances de aparecer na linha 5
                        || linha5_coluna4 == linha4_coluna1
                        || linha5_coluna4 == linha4_coluna2
                        || linha5_coluna4 == linha4_coluna3
                        || linha5_coluna4 == linha4_coluna5
                        || linha5_coluna4 == 0) {
                        linha5_coluna4 += 1;
                    }

                    if (linha5_coluna5 == linha5_coluna1
                        || linha5_coluna5 == linha5_coluna2
                        || linha5_coluna5 == linha5_coluna3
                        || linha5_coluna5 == linha5_coluna4
                        //Evitar chances de aparecer na linha 1
                        || linha5_coluna5 == linha1_coluna1
                        || linha5_coluna5 == linha1_coluna2
                        || linha5_coluna5 == linha1_coluna3
                        || linha5_coluna5 == linha1_coluna4
                        //Evitar chances de aparecer na linha 2
                        || linha5_coluna5 == linha2_coluna1
                        || linha5_coluna5 == linha2_coluna2
                        || linha5_coluna5 == linha2_coluna3
                        || linha5_coluna5 == linha2_coluna4
                        //Evitar chances de aparecer na linha 4
                        || linha5_coluna5 == linha3_coluna1
                        || linha5_coluna5 == linha3_coluna2
                        || linha5_coluna5 == linha3_coluna3
                        || linha5_coluna5 == linha3_coluna4
                        //Evitar chances de aparecer na linha 5
                        || linha5_coluna5 == linha4_coluna1
                        || linha5_coluna5 == linha4_coluna2
                        || linha5_coluna5 == linha4_coluna3
                        || linha5_coluna5 == linha4_coluna4
                        || linha5_coluna5 == 0) {
                        linha5_coluna5 += 1;
                    }

                    /**
                    * Bloco da coluna 5
                    */
                    if (
                        linha1_coluna5 == linha2_coluna5
                        || linha1_coluna5 == linha3_coluna5
                        || linha1_coluna5 == linha4_coluna5
                        || linha1_coluna5 == linha5_coluna5
                        || linha1_coluna5 == 0
                    ) {
                        linha1_coluna5 += 1;
                    }

                    if (
                        linha2_coluna5 == linha1_coluna5
                        || linha2_coluna5 == linha3_coluna5
                        || linha2_coluna5 == linha4_coluna5
                        || linha2_coluna5 == linha5_coluna5
                        || linha2_coluna5 == 0
                    ) {
                        linha2_coluna5 += 1;
                    }

                    if (
                        linha3_coluna5 == linha1_coluna5
                        || linha3_coluna5 == linha2_coluna5
                        || linha3_coluna5 == linha4_coluna5
                        || linha3_coluna5 == linha5_coluna5
                        || linha3_coluna5 == 0
                    ) {
                        linha3_coluna5 += 1;
                    }

                    if (
                        linha4_coluna5 == linha1_coluna5
                        || linha4_coluna5 == linha2_coluna5
                        || linha4_coluna5 == linha3_coluna5
                        || linha4_coluna5 == linha5_coluna5
                        || linha4_coluna5 == 0
                    ) {
                        linha4_coluna5 += 1;
                    }

                    if (
                        linha5_coluna5 == linha1_coluna5
                        || linha5_coluna5 == linha2_coluna5
                        || linha5_coluna5 == linha3_coluna5
                        || linha5_coluna5 == linha4_coluna5
                        || linha5_coluna5 == 0
                    ) {
                        linha5_coluna5 += 1;
                    }

                    this.cartelas.push(
                        {
                            bingo: {
                                codigo: Math.floor(Math.random() * 1000000 + 1),
                                linha1: {
                                    coluna1: linha1_coluna1,
                                    coluna2: linha1_coluna2,
                                    coluna3: linha1_coluna3,
                                    coluna4: linha1_coluna4,
                                    coluna5: linha1_coluna5
                                },
                                linha2: {
                                    coluna1: linha2_coluna1,
                                    coluna2: linha2_coluna2,
                                    coluna3: linha2_coluna3,
                                    coluna4: linha2_coluna4,
                                    coluna5: linha2_coluna5
                                },
                                linha3: {
                                    coluna1: linha3_coluna1,
                                    coluna2: linha3_coluna2,
                                    coluna3: linha3_coluna3,
                                    coluna4: linha3_coluna4,
                                    coluna5: linha3_coluna5
                                },
                                linha4: {
                                    coluna1: linha4_coluna1,
                                    coluna2: linha4_coluna2,
                                    coluna3: linha4_coluna3,
                                    coluna4: linha4_coluna4,
                                    coluna5: linha4_coluna5
                                },
                                linha5: {
                                    coluna1: linha5_coluna1,
                                    coluna2: linha5_coluna2,
                                    coluna3: linha5_coluna3,
                                    coluna4: linha5_coluna4,
                                    coluna5: linha5_coluna5
                                },
                            }
                        })
                }
                console.log(countHTMLs);
                cols = '';
                for (let x = 0; x < this.cartelas.length; x++) {
                    cols = cols + '<div class="col-md-6" style="page-break-before: always;">'
                        + '<div class="card mb-4 shadow-sm" style="border: 1px dashed #d7d7d7; border-radius: 0px">'
                        + '<div class="card-body">'

                        + '<div class="row">'
                        + '<div class="col-md-3">'
                        + '<p class="card-text"><strong>Código: ' + this.cartelas[x].bingo.codigo + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<p class="card-text"><strong>Data: ' + this.dateB + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<p class="card-text"><strong>Horário: ' + this.hour + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-3">'
                        + '<p class="card-text"><strong>Valor: R$ ' + this.value + '</strong></p>'
                        + '</div>'
                        + '</div>'
                        + '<hr style="border-top: 1px dashed #d7d7d7;">'
                        + '<div class="row">'
                        + '<div class="col-md-4">'
                        + '<p class="card-text"><strong>Código: ' + this.cartelas[x].bingo.codigo + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-4">'
                        + '<p class="card-text"><strong>Data: ' + this.dateB + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-4">'
                        + '<p class="card-text"><strong>Horário: ' + this.hour + '</strong></p>'
                        + '</div>'
                        + '</div>'
                        + '<hr style="border-top: 1px solid #d7d7d7">'
                        + '<div class="row">'
                        + '<div class="col-md-8">'
                        + '<p class="card-text"><strong>Local: ' + this.locale + '</strong></p>'
                        + '</div>'
                        + '<div class="col-md-4">'
                        + '<p class="card-text"><strong>Valor: R$ ' + this.value + '</strong></p>'
                        + '</div>'
                        + '</div>'
                        + '<br>'

                        + '<div class="d-flex justify-content-between align-items-center">'
                        + '<table class="table table-bordered table-striped colortable">'
                        + '<tr>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna1 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna2 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna3 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna4 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha1.coluna5 + '</h5>'
                        + '</td>'
                        + '</tr>'

                        + '<tr>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna1 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna2 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna3 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna4 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha2.coluna5 + '</h5>'
                        + '</td>'
                        + '</tr>'

                        + '<tr>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna1 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna2 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna3 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna4 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha3.coluna5 + '</h5>'
                        + '</td>'
                        + '</tr>'

                        + '<tr>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna1 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna2 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna3 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna4 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha4.coluna5 + '</h5>'
                        + '</td>'
                        + '</tr>'

                        + '<tr>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna1 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna2 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna3 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna4 + '</h5>'
                        + '</td>'
                        + '<td style="border: solid 3px #d7d7d7">'
                        + '<h5 style="text-align: center">' + this.cartelas[x].bingo.linha5.coluna5 + '</h5>'
                        + '</td>'
                        + '</tr>'
                        + '</table>'

                        + '</div>'
                        + '</div>'
                        + '</div>'
                        + '</div>';
                }

                this.html = '<!DOCTYPE html>'
                    + '<html lang="pt-br">'
                    + '<head>'
                    + '<meta charset="utf-8">'
                    + '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">'
                    + '<link rel="stylesheet" href="bootstrap4/css/bootstrap.min.css" integrity="anonymous" />'
                    + '<link rel="stylesheet" href="bootstrap4/css/docs/vendor/album.css" />'
                    + '<style>'
                    + '.bd-placeholder-img {'
                    + 'font-size: 1.125rem;'
                    + 'text-anchor: middle;'
                    + '}'
                    + '@media (min-width: 768px) {'
                    + '.bd-placeholder-img-lg {'
                    + 'font-size: 3.5rem;'
                    + '}'
                    + '}'
                    + '</style>'
                    + '</head>'
                    + '<body>'
                    + '<main role="main" style="page-break-before: always;">'
                    + '<div class="album py-5 bg-light" style="page-break-before: always;">'
                    + '<div class="container-fluid" style="page-break-before: always;">'
                    + '<div class="row" style="page-break-before: always;">'
                    + '<div class="col-md-12">'
                    + '<section class="jumbotron text-center">'
                    + '<div class="container" style="color: black">'
                    + '<h1 class="jumbotron-heading">' + this.promoter + '</h1>'
                    + '</div>'
                    + '</section>'
                    + '</div>'
                    + cols
                    + '</div>'
                    + '</div>'
                    + '</div>'
                    + '</main>'
                    + '</body>'
                    + '</html>';

                this.file = this.file + this.html;
                console.log(this.file);
            }

            var fs = require('fs');
            let pathTo = path.join(__dirname, '/bingosGenerated.html')
            fs.writeFile(pathTo, this.file, function (err) {
                if (err) {
                    alert('write pdf file error', err);
                } else {
                    const HTML5ToPDF = require("html5-to-pdf");
                    const path = require("path");

                    const run = async () => {
                        const html5ToPDF = new HTML5ToPDF({
                            inputPath: path.join(__dirname, "bingosGenerated.html"),
                            outputPath: path.join(__dirname, "tmp", "output.pdf"),
                            templatePath: path.join(__dirname),
                            include: [

                            ],
                        })

                        await html5ToPDF.start();
                        await html5ToPDF.build();
                        await html5ToPDF.close();
                        console.log("DONE");
                        alert('Cartelas geradas com sucesso...');
                    }
                    run();
                }
            })
        }
    }
});